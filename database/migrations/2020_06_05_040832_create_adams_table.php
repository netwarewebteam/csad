<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cscart_orderid');
            $table->string('BranchCode');
            $table->string('CompanyCode');
            $table->string('Currency');
            $table->string('CustomerCode');
            $table->string('CustomerShipCode');
            $table->string('DeliveryDate');
            $table->string('DistributerCode');
            $table->string('InvoiceDate');
            $table->string('LineTotal');
            $table->string('RoundingAmount');
            $table->string('SalesInvoiceCode');
            $table->string('SalesOrderCode');
            $table->string('SalesManCode');
            $table->string('Status');
            $table->string('TotalDiscount');
            $table->string('TotalGross');
            $table->string('TotalNet');
            $table->string('TotalTax');
            $table->string('WarehouseCode');
            // $table->string('invoiceDetails');
            $table->string('login');
            $table->string('password');
            $table->string('BaseQuantity');
            $table->string('BaseUOM');
            $table->string('ConvRate');
            $table->string('DiscountAmount');
            $table->string('GrossAmount');
            $table->string('ItemCode');
            $table->string('LineNumber');
            $table->string('LineType');
            $table->string('NetAmount');
            $table->string('Quantity');
            $table->string('SupplierCode');
            $table->string('TaxAmount');
            
            $table->string('UOM');
            $table->string('UnitPrice');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adams');
    }
}
